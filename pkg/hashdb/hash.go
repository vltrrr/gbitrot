package hashdb

import (
	"bufio"
	"crypto/sha256"
	"fmt"
	"gitlab.com/vltrrr/gbitrot/pkg/util"
	"io"
	"os"
)

func CalcFileSHA256(path string) *string {
	hash := sha256.New()

	file_handle, err := os.Open(path)
	if os.IsPermission(err) {
		return nil
	}
	util.CheckErr(err)
	defer file_handle.Close()

	reader := bufio.NewReader(file_handle)
	_, err = io.Copy(hash, reader)
	if err != nil {
		return nil
	}

	hash_str := fmt.Sprintf("%64x", hash.Sum(nil))
	return &hash_str
}
