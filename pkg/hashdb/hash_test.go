package hashdb

import (
	"testing"
)

func TestCalcFileSHA256(t *testing.T) {
	test_sum := "9a30a503b2862c51c3c5acd7fbce2f1f784cf4658ccf8e87d5023a90c21c0714"
	hash_sum := CalcFileSHA256("../../testdata/testfile.txt")
	if *hash_sum != test_sum {
		t.Error("sha256 mismatch")
	}
}
