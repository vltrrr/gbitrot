package hashdb

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/vltrrr/gbitrot/pkg/util"
	"time"
)

// ____________________________________________________________________________
// global

var (
	dbCurrentTx     *sql.Tx
	dbCurrentTxTime int64
)

const (
	DB_NAME        string = "gbitrot.db"
	DB_TX_INTERVAL int64  = 3
)

// ____________________________________________________________________________
// DB construction

func DbOpen() *sql.DB {
	db, err := sql.Open("sqlite3", DB_NAME)
	util.CheckErr(err)

	create_sql := `
    create table if not exists files (
        id integer not null primary key,
        path text,
        hash text,
        mtime integer
    );
    CREATE UNIQUE INDEX if not exists path_index ON files(path);
    CREATE INDEX if not exists hash_index ON files(hash);
    `
	_, err = db.Exec(create_sql)
	util.CheckErr(err)

	return db
}

// ____________________________________________________________________________
// transactions

func dbOpenTx(db *sql.DB) {
	var err error
	dbCurrentTx, err = db.Begin()
	util.CheckErr(err)
	dbCurrentTxTime = time.Now().Unix()
}

func DbGetCurrentTx() *sql.Tx {
	return dbCurrentTx
}

func DbTxCommitMaybe(db *sql.DB, final_commit bool) {
	// ensures there is a tx active, which is committed periodically.
	// new tx is created unless final_commit == true

	if dbCurrentTx == nil {
		dbOpenTx(db)
	} else {
		if (time.Now().Unix()-dbCurrentTxTime) >= DB_TX_INTERVAL || final_commit {
			err := dbCurrentTx.Commit()
			util.CheckErr(err)
			if !final_commit {
				dbOpenTx(db)
			}
		}
	}
}

// ____________________________________________________________________________
// Finders

func DbFindPath(tx *sql.Tx, path string) (*string, *int64) {
	rows, err := tx.Query("select hash, mtime from files where path = ?", path)
	util.CheckErr(err)

	for rows.Next() {
		var hash string
		var mtime int64
		err = rows.Scan(&hash, &mtime)
		util.CheckErr(err)
		return &hash, &mtime
	}
	return nil, nil
}

func DbFindHash(tx *sql.Tx, hash string) *sql.Rows {
	rows, err := tx.Query("SELECT hash, path, mtime FROM files WHERE hash LIKE ?", hash+"%")
	util.CheckErr(err)
	return rows
}

func DbListPaths(tx *sql.Tx) *[]string {
	rows, err := tx.Query("select path from files")
	util.CheckErr(err)

	path_slice := make([]string, 0)
	for rows.Next() {
		var path string
		err = rows.Scan(&path)
		util.CheckErr(err)
		path_slice = append(path_slice, path)
	}
	return &path_slice
}

// ____________________________________________________________________________
// Create/update/delete

func DbInsertHash(tx *sql.Tx, path string, hash string, mtime int64) {
	_, err := tx.Exec("insert into files(path, hash, mtime) values(?, ?, ?)", path, hash, mtime)
	util.CheckErr(err)
}

func DbUpdateHash(tx *sql.Tx, path string, hash string, mtime int64) {
	_, err := tx.Exec("UPDATE files SET hash = ?, mtime = ? WHERE path = ?", hash, mtime, path)
	util.CheckErr(err)
}

func DbDelHash(tx *sql.Tx, path string) {
	_, err := tx.Exec("DELETE FROM files WHERE path = ?", path)
	util.CheckErr(err)
}
