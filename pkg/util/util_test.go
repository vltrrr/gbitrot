package util

import (
	"fmt"
	"testing"
)

func TestCheckFileExists(t *testing.T) {
	if CheckFileExists("./does_not_exist") == true {
		t.Error("file does not exist")
	}
	if CheckFileExists("../../testdata/testfile.txt") == false {
		t.Error("file does exist")
	}
}

func TestBytesToHumanReadable(t *testing.T) {
	fmt.Println(BytesToHumanReadable(1024))
	if BytesToHumanReadable(1024) != "1.0 KiB" {
		t.Error("KiB fail")
	}
	if BytesToHumanReadable(1024*1024*99+1024*100) != "99.1 MiB" {
		t.Error("MiB fail")
	}
}
