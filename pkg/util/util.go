package util

import (
	"fmt"
	"log"
	"os"
)

// ____________________________________________________________________________
// error handling

func CheckErr(err error) {
	if err != nil {
		fmt.Print("\n*** Unexpected error ***\n")
		log.Fatal(err)
	}
}

// ____________________________________________________________________________
// file

func CheckFileExists(path string) bool {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func BytesToHumanReadable(size int64) string {
	const kilo = 1024
	if size < kilo {
		return fmt.Sprintf("%d B", size)
	}
	divisor := int64(kilo)
	exponent := 0
	for n := size / kilo; n >= kilo; n /= kilo {
		divisor *= kilo
		exponent++
	}
	human_size := float64(size) / float64(divisor)
	human_unit := fmt.Sprintf("%ciB", "KMGTPE"[exponent])
	return fmt.Sprintf("%.1f %s", human_size, human_unit)
}
