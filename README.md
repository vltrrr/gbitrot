# gbitrot

![alt text](img/screenshot.png "gbitrot")

`gbitrot` is a simple Bitrot detector made with Go, inspired by the similar python tool `bitrot` by Łukasz Langa and others (https://github.com/ambv/bitrot/).

When run, `gbitrot` traverses the filesystem tree, starting from current working directory, calculating SHA256 hash for each file it finds. 
The hashes are stored in a SQLite database stored on the working directory (`gbitrot.db`).
On the next run, it re-calculates hashes and if it the calculated hash mismatches the one found in the DB, it prints a Big Red Warning™, unless ofc the file has been legitimately modified meanwhile (newer mtime).

The software should be quite fast, utilizing goroutines for concurrency. Speed should be mostly disk bound on basic SATA SSD:s, for faster (M.2 NVMe disks etc) it is probably CPU bound, however you can add more threads with `-t` parameter. 
For spinning disks you may want to use only single thread calculating the hashes (`gbitrot -t 1`) to yield more sequential disk access.

## Uses

It can be used as a bitrot detector on local disks or media, or just create a database on contents with SHA256 hashes (it is a single table SQLite DB after all), usable with other tools. For me it was learning project for Golang, but also a personally useful tool for some usecases.

## Installation

You need go, probably anything higher than >1.14 works. Tested with `go1.16 linux/amd64`

```
go install gitlab.com/vltrrr/gbitrot/cmd/gbitrot@latest
```

## Usage

Basic run, does not print anything unless finds errors:
```
gbitrot
```

Print traversed files and their hashes/status, traverse only fs tree under `Downloads`:
```
gbitrot -v Downloads/
```

Search the DB for files with hash starting with `4a8190b1`:
```
gbitrot -f 4a8190b1
```

Clean database from deleded/non-present files. `gbitrot` will not delete hashes by default, and as such survives temporary unavailable files (removable disks)
```
gbitrot -c
```

### All options:

```
Usage of gbitrot:
  -c	Clean database, remove missing files
  -f string
    	Find/search hash
  -t int
    	Threads (default 8)
  -v	Verbose, prints hashing progress
  -version
    	Display version
```

## Changelog

### v0.6.0

Internal reorganiztion to modules & cleanup

### v0.5.2

Add stats printout in the end of runs.

### v0.5.1

Add concurrency using goroutines.

### v0.4.4

First public release.
