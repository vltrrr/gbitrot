package main

import (
	"fmt"
	"gitlab.com/vltrrr/gbitrot/pkg/util"
)

const (
	COLOR_GREEN = 46
	COLOR_BLUE  = 51
	COLOR_RED   = 197
)

func printHashLine(msg string, color int, path string, indent bool) {
	if indent {
		fmt.Printf("\033[38;5;%dm%64s\033[39;49m  %s\n", color, msg, path)
	} else {
		fmt.Printf("\033[38;5;%dm%s\033[39;49m  %s\n", color, msg, path)
	}
}

func printScanReport(stats scanStats) {
	size_human := util.BytesToHumanReadable(stats.total_size)
	fmt.Printf("# Scan finnished, checked %d files, %s\n", stats.n_of_files, size_human)
	if len(stats.mismatch_files) > 0 {
		fmt.Printf("# SHA256 MISMATCH detected on %d files:\n", len(stats.mismatch_files))
		for _, path := range stats.mismatch_files {
			fmt.Printf("# %s\n", path)
		}
	} else {
		fmt.Println("# No errors detected.")
	}
}
