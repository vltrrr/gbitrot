package main

import (
	"fmt"
	"gitlab.com/vltrrr/gbitrot/pkg/hashdb"
)

// ____________________________________________________________________________
// global

const (
	VERSION     string = "0.6.0 2021_02_19"
	MAX_THREADS int    = 64
)

// ____________________________________________________________________________
// main

func main() {
	args := parseArgs()

	if args.version {
		fmt.Printf("gbitrot %s\n", VERSION)
		return
	}

	db := hashdb.DbOpen()

	if args.search != "" {
		findHashMain(db, args.search)
	} else if args.clean {
		cleanDbMain(db, args.verbose)
	} else {
		scanMain(db, args.root_path, args.threads, args.verbose)
	}
}
