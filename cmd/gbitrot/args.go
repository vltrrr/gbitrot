package main

import (
	"flag"
)

type gbitrot_args struct {
	verbose   bool
	clean     bool
	search    string
	root_path string
	threads   int
	version   bool
}

func parseArgs() *gbitrot_args {
	verbosePtr := flag.Bool("v", false, "Verbose, prints hashing progress")
	cleanPtr := flag.Bool("c", false, "Clean database, remove missing files")
	searchPtr := flag.String("f", "", "Find/search hash")
	threadsPtr := flag.Int("t", 8, "Threads")
	versionPtr := flag.Bool("version", false, "Display version")

	flag.Parse()

	args := gbitrot_args{
		verbose:   *verbosePtr,
		clean:     *cleanPtr,
		search:    *searchPtr,
		root_path: ".",
		threads:   *threadsPtr,
		version:   *versionPtr,
	}

	tail_args := flag.Args()
	if len(tail_args) > 0 {
		args.root_path = tail_args[0]
	}

	if args.threads < 1 {
		args.threads = 1
	} else if args.threads > MAX_THREADS {
		args.threads = MAX_THREADS
	}

	return &args
}
