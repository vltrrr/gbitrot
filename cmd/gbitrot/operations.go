package main

import (
	"database/sql"
	"gitlab.com/vltrrr/gbitrot/pkg/hashdb"
	"gitlab.com/vltrrr/gbitrot/pkg/util"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)

// ____________________________________________________________________________
// scan

type fileHash struct {
	path  string
	hash  string
	mtime int64
	size  int64
}

type scanStats struct {
	n_of_files     int64
	total_size     int64
	mismatch_files []string
}

func newScanStats() scanStats {
	stats := scanStats{}
	stats.n_of_files = 0
	stats.total_size = 0
	stats.mismatch_files = make([]string, 0)
	return stats
}

func scanMain(db *sql.DB, path string, threads int, verbose bool) {
	paths_chan := make(chan string)
	fhash_chan := make(chan fileHash)
	var hash_wg sync.WaitGroup
	var check_wg sync.WaitGroup

	stats := newScanStats()

	// single goroutine scanning the fs tree
	go func() {
		walkTree(path, paths_chan)
		close(paths_chan)
	}()

	// multiple goroutines calculating hashes
	for i := 0; i < threads; i++ {
		hash_wg.Add(1)
		go func() {
			calcFileHash(paths_chan, fhash_chan)
			hash_wg.Done()
		}()
	}

	// check/add hashes to DB generated
	check_wg.Add(1)
	go func() {
		for fhash := range fhash_chan {
			hashdb.DbTxCommitMaybe(db, false)
			hash_ok := checkFileHash(&fhash, verbose)
			if !hash_ok {
				stats.mismatch_files = append(stats.mismatch_files, fhash.path)
			}
			stats.n_of_files++
			stats.total_size += fhash.size
		}
		check_wg.Done()
	}()

	// after hashing is done, close the fileHash channel
	// to end hash checking
	hash_wg.Wait()
	close(fhash_chan)

	// after hash checking is also done, do final commit
	check_wg.Wait()
	hashdb.DbTxCommitMaybe(db, true)

	printScanReport(stats)
}

func walkTree(path string, paths_chan chan<- string) {
	path_info, err := os.Lstat(path)
	if err != nil {
		// if cannot stat, skip file
		return
	}

	if !path_info.IsDir() && path_info.Mode().IsRegular() {
		// regular file
		if path_info.Name() == hashdb.DB_NAME {
			return
		}
		paths_chan <- path
	} else {
		// dir, list & recurse deeper
		dir_files, err := ioutil.ReadDir(path)
		if err != nil {
			return
		}

		for _, file := range dir_files {
			file_full_path := filepath.Join(path, file.Name())
			walkTree(file_full_path, paths_chan)
		}
	}
}

func calcFileHash(paths_chan <-chan string, fhash_chan chan<- fileHash) {
	for path := range paths_chan {
		file_info, err := os.Lstat(path)
		if err != nil {
			continue
		}
		hash_file := hashdb.CalcFileSHA256(path)
		if hash_file == nil {
			continue
		}
		fhash := fileHash{path: path}
		fhash.hash = *hash_file
		fhash.mtime = file_info.ModTime().UnixNano()
		fhash.size = file_info.Size()
		fhash_chan <- fhash
	}
}

func checkFileHash(fhash *fileHash, verbose bool) bool {
	tx := hashdb.DbGetCurrentTx()
	hash_db, mtime_db := hashdb.DbFindPath(tx, fhash.path)

	if hash_db == nil || mtime_db == nil {
		// new file
		if verbose {
			printHashLine("new", COLOR_GREEN, fhash.path, true)
		}
		hashdb.DbInsertHash(tx, fhash.path, fhash.hash, fhash.mtime)
		return true
	}
	if fhash.mtime > *mtime_db {
		// existing file, but modified
		if verbose {
			printHashLine("modified", COLOR_GREEN, fhash.path, true)
		}
		hashdb.DbUpdateHash(tx, fhash.path, fhash.hash, fhash.mtime)
		return true
	}
	if *hash_db != fhash.hash {
		// corrupt
		printHashLine("### SHA256 MISMATCH ###", COLOR_RED, fhash.path, verbose)
		return false
	}
	if verbose {
		printHashLine(*hash_db, COLOR_BLUE, fhash.path, true)
	}
	return true
}

// ____________________________________________________________________________
// clean

func cleanDbMain(db *sql.DB, verbose bool) {
	tx, err := db.Begin()
	util.CheckErr(err)

	path_slice := hashdb.DbListPaths(tx)

	for i := 1; i < len(*path_slice); i++ {
		path := (*path_slice)[i]
		if !util.CheckFileExists(path) {
			if verbose {
				printHashLine("removed", COLOR_RED, path, false)
			}
			hashdb.DbDelHash(tx, path)
		}
	}

	err = tx.Commit()
	util.CheckErr(err)
}

// ____________________________________________________________________________
// find

func findHashMain(db *sql.DB, hash string) {
	tx, err := db.Begin()
	util.CheckErr(err)

	rows := hashdb.DbFindHash(tx, hash)
	for rows.Next() {
		var hash string
		var path string
		var mtime int64
		err = rows.Scan(&hash, &path, &mtime)
		util.CheckErr(err)
		printHashLine(hash, COLOR_BLUE, path, true)
	}

	err = tx.Commit()
	util.CheckErr(err)
}
